class CreateTwiiterhandles < ActiveRecord::Migration
  def change
    create_table :twiiterhandles do |t|
    	t.string :twiiter_handle
    	t.string :tweet
    	t.datetime :tweet_date
    	t.integer :tweet_id ,:limit=> 8

      t.timestamps
    end
  end
end
